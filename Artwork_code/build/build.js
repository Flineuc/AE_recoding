var colors = [
    "#68B945",
    "#2EB14A",
    "#228B4C",
    "#017E59",
    "#0D6987",
    "#0C559F",
    "#28439C",
    "#262260",
    "#761C57",
    "#B31F29",
    "#DC2327",
    "#EA3A25",
    "#E08126",
    "#DFA22A",
    "#EADC35",
];
var width_rect = [2, 4, 8, 16, 8, 4, 2, 1, 2, 4, 8, 16, 8, 4, 2];
var offset_plus = [1, 2, 3, 4];
var offset_moins = [-1, -2, -3, -4];
var t = 0;
var save_on = false;
var choice = 0;
var img;
function sum(tab, index) {
    var sum = 0;
    for (var i = 0; i < index; i++) {
        sum = sum + tab[i];
    }
    return sum;
}
var state = {
    off_set: 0,
    i: 0,
};
function keyTyped(keyEvent) {
    if (!keyEvent.repeat) {
        state = try_line(state);
        state.i++;
    }
}
function mouseClicked() {
    if (mouseY > (height / 2 + 50) && mouseY < (height / 2 + 100)) {
        if (mouseX < (width / 2 - 70 + 100) && mouseX > (width / 2 - 70)) {
            save_on = true;
            choice++;
        }
        if (mouseX < (width / 2 + 70 + 100) && mouseX > (width / 2 + 70)) {
            save_on = false;
            choice++;
        }
    }
}
function preload() {
    img = loadImage('assets/fond.jpg');
}
function menu() {
    image(img, 0, 0, width, height);
    fill('black');
    noStroke();
    textSize(30);
    textAlign(CENTER);
    var m1 = 'Create your own "15 systematic color rows with vertical condensation" :';
    text(m1, 0, 50, width, 100);
    textSize(20);
    noStroke();
    var m2 = 'Choose with left and right arrow which line do you want and press space to confirm you choice. Then, do it again until you picture is complete !';
    text(m2, 0, 160, width, 100);
    fill('black');
    textSize(20);
    noStroke();
    textAlign(CENTER);
    var m3 = 'Choose if you want to download it once complete and start drawing :';
    text(m3, width / 2 - 200, 290, 400, 150);
    fill('black');
    textSize(30);
    text('YES           NO', width / 2 - 200, height / 2 + 50, 400, 50);
}
function try_line(state) {
    if (keyIsPressed) {
        if (keyCode === LEFT_ARROW) {
            state.off_set = state.off_set - 1;
            var t_1 = 0;
            while (t_1 < 15) {
                fill(colors[(t_1 + 60 + state.off_set) % 15]);
                noStroke();
                rect(sum(width_rect, t_1) * (width / 89), state.i * height / 15, width_rect[t_1] * (width / 89), height / 15);
                t_1++;
            }
        }
        if (keyCode === RIGHT_ARROW) {
            state.off_set = state.off_set + 1;
            var t_2 = 0;
            while (t_2 < 15) {
                fill(colors[(t_2 + 60 + state.off_set) % 15]);
                noStroke();
                rect(sum(width_rect, t_2) * (width / 89), state.i * height / 15, width_rect[t_2] * (width / 89), height / 15);
                t_2++;
            }
        }
    }
    return state;
}
function draw() {
    if (choice === 0) {
        menu();
    }
    if (choice === 1) {
        background('white');
        choice++;
    }
    try_line(state);
    if (state.i === 15 && save_on === true) {
        save();
    }
}
function setup() {
    p6_CreateCanvas();
}
function windowResized() {
    p6_ResizeCanvas();
}
var __ASPECT_RATIO = 1;
var __MARGIN_SIZE = 25;
function __desiredCanvasWidth() {
    var windowRatio = windowWidth / windowHeight;
    if (__ASPECT_RATIO > windowRatio) {
        return windowWidth - __MARGIN_SIZE * 2;
    }
    else {
        return __desiredCanvasHeight() * __ASPECT_RATIO;
    }
}
function __desiredCanvasHeight() {
    var windowRatio = windowWidth / windowHeight;
    if (__ASPECT_RATIO > windowRatio) {
        return __desiredCanvasWidth() / __ASPECT_RATIO;
    }
    else {
        return windowHeight - __MARGIN_SIZE * 2;
    }
}
var __canvas;
function __centerCanvas() {
    __canvas.position((windowWidth - width) / 2, (windowHeight - height) / 2);
}
function p6_CreateCanvas() {
    __canvas = createCanvas(__desiredCanvasWidth(), __desiredCanvasHeight());
    __centerCanvas();
}
function p6_ResizeCanvas() {
    resizeCanvas(__desiredCanvasWidth(), __desiredCanvasHeight());
    __centerCanvas();
}
var p6_SaveImageSequence = function (durationInFrames, fileExtension) {
    if (frameCount <= durationInFrames) {
        noLoop();
        var filename_1 = nf(frameCount - 1, ceil(log(durationInFrames) / log(10)));
        var mimeType = (function () {
            switch (fileExtension) {
                case 'png':
                    return 'image/png';
                case 'jpeg':
                case 'jpg':
                    return 'image/jpeg';
            }
        })();
        __canvas.elt.toBlob(function (blob) {
            p5.prototype.downloadFile(blob, filename_1, fileExtension);
            setTimeout(function () { return loop(); }, 100);
        }, mimeType);
    }
};
//# sourceMappingURL=../src/src/build.js.map