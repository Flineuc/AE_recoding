// -------------------
//  Parameters and UI
// -------------------

const gui = new dat.GUI()
const params = {
    Random_Seed: 0,
    Download_Image: () => save(),
}
gui.add(params, "Random_Seed", 0, 100, 1)
gui.add(params, "Download_Image")


let colors = [
    "#68B945",
    "#2EB14A",
    "#228B4C",
    "#017E59",
    "#0D6987",
    "#0C559F",
    "#28439C",
    "#262260",
    "#761C57",
    "#B31F29",
    "#DC2327",
    "#EA3A25",
    "#E08126",
    "#DFA22A",
    "#EADC35",
]

const width_rect = [2,4,8,16,8,4,2,1,2,4,8,16,8,4,2]
let offset = [-1,-2,-3,-4,2,3]
let off_set = 0
let t = 0

function sum (tab, index){
    let sum=0
    for (let i =0 ; i < index ; i++){
        sum=sum+tab[i]
    }
    return sum
}

let img;
let artwork;
let choice = 0

function preload() {
  img = loadImage('assets/fond.jpg');
  artwork = loadImage('assets/artwork.jpg');
}

function mouseClicked() {
    if (mouseY > 270 && mouseY < 270 + height/4 ){
        if (mouseX > height/2+70 && mouseX < height/2+70 + width/4 ) choice ++
    }
}

function menu(){
    fill('black')
    stroke(10);
    textSize(30);
    textAlign(CENTER);
    let m1='Recoding Work'
    text(m1,0,50,width,100);

    noStroke()
    textSize(20);
    let m11='The first part of Algorithmic Aesthetics project was to recode an Artwork from 1970. I chose "15 systematic color rows with vertical condensation" realized by Richard Paul Lohse.'
    let m12='Indeed, I had to write a code to display this random artwork :'
    text(m11,0,100,width,100);
    text(m12,0,220,width,100);

    textSize(20)
    noStroke()
    let m2= 'Click on it to see my version ==>'
    let m3='Then, you can change the "Random_Seed" parameter to see random variations.'
    text(m2,50,height/2-60,width/2,100);
    text(m3,50,height/2-20,width/2,100);

}

// -------------------
//       Drawing
// -------------------

function draw() {
    if (choice===0){
        image(img, 0, 0, width, height);
        image(artwork, height/2+70, 270, width/4, height/4);
        menu()
    }
    else{
        choice++
        randomSeed(params.Random_Seed)
        for (let i = 0; i < 15; i ++){
            if (i==0) off_set=0
            else off_set += random(offset)
            let t = 0
            
            while (t<15){
                    fill(colors[(t+60+off_set) % 15 ])
                    noStroke()
                    rect(sum(width_rect,t)*(width/89),
                        i*height/15,
                        width_rect[t]*(width/89),
                        height/15)
                    t++
            }
        }
    
    }   
}

// -------------------
//    Initialization
// -------------------

function setup() {
    p6_CreateCanvas()
}

function windowResized() {
    p6_ResizeCanvas()
}