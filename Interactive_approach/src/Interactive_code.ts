// -------------------
//  Parameters and UI
// -------------------

let colors = [
    "#68B945",
    "#2EB14A",
    "#228B4C",
    "#017E59",
    "#0D6987",
    "#0C559F",
    "#28439C",
    "#262260",
    "#761C57",
    "#B31F29",
    "#DC2327",
    "#EA3A25",
    "#E08126",
    "#DFA22A",
    "#EADC35",
]

const width_rect = [2,4,8,16,8,4,2,1,2,4,8,16,8,4,2]
let offset_plus = [1,2,3,4]
let offset_moins= [-1,-2,-3,-4]
let t = 0

let save_on=false
let choice = 0
let img;

function sum (tab, index){
    let sum=0
    for (let i =0 ; i < index ; i++){
        sum=sum+tab[i]
    }
    return sum
}

interface State {
    off_set: number,
    i: number,
}

let state: State = {
    off_set: 0,
    i: 0,
}

function keyTyped (keyEvent) {
    if(!keyEvent.repeat) {
        state = try_line (state)
        state.i++
    }
}

function mouseClicked() {
    if (mouseY > (height/2+50) && mouseY < (height/2+100)) {
        if (mouseX < (width/2-70+100) && mouseX > (width/2-70)) {
            save_on=true
            choice++
        }
        if (mouseX < (width/2+70+100) && mouseX > (width/2+70) ) {
            save_on=false
            choice++
        }
    }
}

function preload() {
  img = loadImage('assets/fond.jpg');
}

function menu(){
    image(img, 0, 0, width, height);
    fill('black')
    noStroke()
    textSize(30);
    textAlign(CENTER);
    let m1='Create your own "15 systematic color rows with vertical condensation" :'
    text(m1,0,50,width,100);

    textSize(20)
    noStroke()
    let m2= 'Choose with left and right arrow which line do you want and press space to confirm you choice. Then, do it again until you picture is complete !'
    text(m2,0,160,width,100);

    fill ('black')
    textSize(20);
    noStroke()
    textAlign(CENTER);
    let m3 = 'Choose if you want to download it once complete and start drawing :'
    text(m3,width/2-200,290,400,150);
    fill('black');
    textSize(30);
    text('YES           NO', width/2-200, height/2+50, 400,50);
}

function try_line (state: State): State{
    if (keyIsPressed){
                if (keyCode === LEFT_ARROW) {
                    state.off_set = state.off_set-1
                    let t = 0
                    while (t<15){
                        fill(colors[(t+60+state.off_set) % 15 ])
                            noStroke()
                            rect(sum(width_rect,t)*(width/89),
                            state.i*height/15,
                            width_rect[t]*(width/89),
                            height/15)
                            t++
                    }
                }
                if (keyCode === RIGHT_ARROW) {
                    state.off_set  = state.off_set+1
                    let t = 0
                    while (t<15){
                        fill(colors[(t+60+state.off_set) % 15 ])
                            noStroke()
                            rect(sum(width_rect,t)*(width/89),
                            state.i*height/15,
                            width_rect[t]*(width/89),
                            height/15)
                            t++
                    }
                }
    } return state
}

// -------------------
//       Drawing
// -------------------

function draw() {
    if (choice===0){ menu()}
    if (choice===1) {
        background('white')
        choice++
    }
    try_line (state);
    if (state.i===15 && save_on===true) {
        save()
    }
}

// -------------------
//    Initialization
// -------------------

function setup() {
    p6_CreateCanvas()
}

function windowResized() {
    p6_ResizeCanvas()
}

